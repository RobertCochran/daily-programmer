(defparameter *tile-counts*
  '((a . 9)
    (b . 2)
    (c . 2)
    (d . 4)
    (e . 12)
    (f . 2)
    (g . 3)
    (h . 2)
    (i . 9)
    (j . 1)
    (k . 1)
    (l . 4)
    (m . 2)
    (n . 6)
    (o . 8)
    (p . 2)
    (q . 1)
    (r . 6)
    (s . 4)
    (t . 6)
    (u . 4)
    (v . 2)
    (w . 2)
    (x . 1)
    (y . 2)
    (z . 1)
    (_ . 2)))

(defun remove-tiles (tile-string)
  (let ((new-tile-counts (copy-tree *tile-counts*)))
    (loop for tile across tile-string do
	 (let ((tile-alist (assoc (intern (string tile)) new-tile-counts)))
	   (when (minusp (decf (cdr tile-alist)))
	     (format t "Invalid input. Too many '~a's.~%" (car tile-alist))
	     (return-from remove-tiles))))
    new-tile-counts))

(defun print-tile-counts (tile-counts)
  (if (null tile-counts) (return-from print-tile-counts))
  (let ((max-remaining (reduce #'max (mapcar #'cdr tile-counts))))
    (loop for x from max-remaining downto 0 do
	 (let ((letters-for-x
		(mapcar #'car (remove x tile-counts :test-not #'= :key #'cdr))))
	   (if (plusp (length letters-for-x))
	       (format t "~a: ~{~a~^, ~}~%" x letters-for-x))))
    (terpri)))

(dolist (x '("AEERTYOXMCNB_S"
	     "PQAREIOURSTHGWIOAE_"
	     "LQTOONOEFFJZT"
	     "AXHDRUIOR_XHJZUQEE"))
  (print-tile-counts (remove-tiles x)))
