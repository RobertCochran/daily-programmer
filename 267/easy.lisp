;; Displays a list like 1st, 2nd, 3rd...

(defun not-win-places-decimal (placement &optional (range 100))
  (loop for i from 1 to range do
       (block my-continue
	 (if (= i placement) (return-from my-continue))
	 (let ((word (format nil "~:r" i)))
	   (format t "~d~a, " i (subseq word (- (length word) 2)))))))

;; Displays a list like first, second, third...

(defun not-win-places-words (placement &optional (range 100))
  (loop for i from 1 to range do
       (block my-continue
	 (if (= i placement) (return-from my-continue))
	 (format t "~:r, " i))))

;; Use as such

(not-win-places-decimal 6)
;; or
(not-win-places-decimal 91 1000)
;; or
(not-win-places-words 42)
;; or
(not-win-places-words 18 500)

