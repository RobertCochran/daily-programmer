(defun balancedp (seq)
  (unless (= 1 (length seq))
    (loop for item across (remove-duplicates seq :test #'char=)
       collect (count item seq :test #'char=) into counts
       always (apply #'= counts))))

(balancedp "xxxyyy") ; => T
(balancedp "yyyxxx") ; => T
(balancedp "xxxyyyy") ; => NIL
(balancedp "yyxyxxyxxyyyyxxxyxyx") ; => T
(balancedp "xyxxxxyyyxyxxyxxyy") ; => NIL
(balancedp "") ; => T
(balancedp "x") ; => NIL

(balancedp "xxxyyyzzz") ; => T
(balancedp "abccbaabccba") ; => T
(balancedp "xxxyyyzzzz") ; => NIL
(balancedp "abcdefghijklmnopqrstuvwxyz") ; => T
(balancedp "pqq") ; => NIL
(balancedp "fdedfdeffeddefeeeefddf") ; => NIL
(balancedp "www") ; => T
(balancedp "") ; => T



