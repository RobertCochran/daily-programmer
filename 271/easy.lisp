(defun probability-of-kill (sides health)
  (* (/ (expt sides (floor health sides)))
     (if (> (mod health sides) 0)
	 (/ (1+ (- sides (mod health sides))) sides)
	 1)))

(progn
  (dolist (x '((4 1)
	       (4 4)
	       (4 5)
	       (4 6)
	       (1 10)
	       (100 200)
	       (8 20)))
    (format t "~a sides, ~a health -> ~f% probability~%"
	    (first x)
	    (second x)
	    (* 100 (probability-of-kill (first x) (second x)))))
  (terpri))
