(defun create-node-table (size)
  (make-array (list size size) :element-type 'integer :initial-element 0))

(defun add-node-link (table node-a node-b)
  (let ((node-a (1- node-a))
	(node-b (1- node-b)))
    (setf (aref table node-a node-b) 1
	  (aref table node-b node-a) 1)))

(defun split-seq (seq splitp)
  (loop
     for beg = (position-if-not splitp seq)
     then (position-if-not splitp seq :start (1+ end))
     for end = (position-if splitp seq :start beg)
     if beg collect (subseq seq beg end)
     while end))

(defun split-lines (seq)
  (split-seq seq (lambda (x) (char= x #\Newline))))

(defun split-spaces (seq)
  (split-seq seq (lambda (x) (char= x #\Space))))

(defun build-table-from-input (input)
  (let* ((input-lines (split-lines input))
	 (input-size (parse-integer (car input-lines)))
	 (input-lists (mapcar (lambda (x) (mapcar #'parse-integer x))
			      (mapcar #'split-spaces (cdr input-lines))))
	 (output-table (create-node-table input-size)))
    (loop for pair in input-lists do
	 (add-node-link output-table (nth 0 pair) (nth 1 pair)))
    output-table))

;; Because we can't iterate over 2D arrays for some reason...
(defun convert-table-to-lists (table)
  (loop for y below (array-dimension table 0) collecting
       (loop for x below (array-dimension table 1) collecting
	    (aref table y x))))

(defun display-table-info (table)
  (let ((table (convert-table-to-lists table)))
    (loop
       for i below (length table)
       for a in table
       do (format t "Node ~a has ~a links~%" (1+ i) (count-if #'oddp a)))
    (format t "~%Connection matrix:~%~{~{~a ~}~%~}" table)))

(defvar *challenge-input* "16
1 2
1 3
2 3
1 4
3 4
1 5
2 5
1 6
2 6
3 6
3 7
5 7
6 7
3 8
4 8
6 8
7 8
2 9
5 9
6 9
2 10
9 10
6 11
7 11
8 11
9 11
10 11
1 12
6 12
7 12
8 12
11 12
6 13
7 13
9 13
10 13
11 13
5 14
8 14
12 14
13 14
1 15
2 15
5 15
9 15
10 15
11 15
12 15
13 15
1 16
2 16
5 16
6 16
11 16
12 16
13 16
14 16
15 16")

;; To get the output
(display-table-info (build-table-from-input *challenge-input*))

;; Which outputs :
;; Node 1 has 8 links
;; Node 2 has 8 links
;; Node 3 has 6 links
;; Node 4 has 3 links
;; Node 5 has 7 links
;; Node 6 has 10 links
;; Node 7 has 7 links
;; Node 8 has 7 links
;; Node 9 has 7 links
;; Node 10 has 5 links
;; Node 11 has 9 links
;; Node 12 has 8 links
;; Node 13 has 8 links
;; Node 14 has 5 links
;; Node 15 has 9 links
;; Node 16 has 9 links
;;
;; Connection matrix:
;; 0 1 1 1 1 1 0 0 0 0 0 1 0 0 1 1
;; 1 0 1 0 1 1 0 0 1 1 0 0 0 0 1 1
;; 1 1 0 1 0 1 1 1 0 0 0 0 0 0 0 0
;; 1 0 1 0 0 0 0 1 0 0 0 0 0 0 0 0
;; 1 1 0 0 0 0 1 0 1 0 0 0 0 1 1 1
;; 1 1 1 0 0 0 1 1 1 0 1 1 1 0 0 1
;; 0 0 1 0 1 1 0 1 0 0 1 1 1 0 0 0
;; 0 0 1 1 0 1 1 0 0 0 1 1 0 1 0 0
;; 0 1 0 0 1 1 0 0 0 1 1 0 1 0 1 0
;; 0 1 0 0 0 0 0 0 1 0 1 0 1 0 1 0
;; 0 0 0 0 0 1 1 1 1 1 0 1 1 0 1 1
;; 1 0 0 0 0 1 1 1 0 0 1 0 0 1 1 1
;; 0 0 0 0 0 1 1 0 1 1 1 0 0 1 1 1
;; 0 0 0 0 1 0 0 1 0 0 0 1 1 0 0 1
;; 1 1 0 0 1 0 0 0 1 1 1 1 1 0 0 1
;; 1 1 0 0 1 1 0 0 0 0 1 1 1 1 1 0
