(defun load-dictionary ()
  (with-open-file (dict-stream #P"enable1.txt")
    (loop
       for line = (read-line dict-stream nil nil)
       ;; I'm on Linux and this file is CRLF, so strip the CR
       while line collect (subseq line 0 (1- (length line))))))

(defparameter *dictionary-cache* (load-dictionary))

(defun remove-adjacent-letter-duplicates (string)
  (concatenate 'string (loop
                          for prev = #\Space then c
                          for c across string
                          if (char/= c prev) collect c)))

(defun find-swype-matches (string)
  (let ((possible-words
         (remove-if-not (lambda (x)
                          (and (char= (char string 0) (char x 0))
                               (char= (char string (1- (length string)))
                                      (char x (1- (length x))))
                               (<= 5 (length x))))
                        *dictionary-cache*)))
    (remove-if-not (lambda (input)
                     (let ((input (remove-adjacent-letter-duplicates input)))
                       (loop
                          for i below (length input)
                          for last-pos = 0
                          then (position c string :start last-pos)
                          for c = (char input i)
                          always (position c string :start last-pos))))
                   possible-words)))

(defun print-swype-matches (string)
  (let ((matches (find-swype-matches string)))
    (if matches
        (format t "Matches for ~a: ~{~a~^, ~}~%" string matches)
        (format t "No matches for ~a~%" string))))

(print-swype-matches "qwertyuytresdftyuioknn")
(print-swype-matches "gijakjthoijerjidsdfnokg")
;; String that matches for the bonus: matches
;; 'pollywog' while containing only 1 'L' character
(print-swype-matches "polkjypwyohg")
;; More inputs, because I felt like it
(print-swype-matches "rtyuiokjnbhjijn")
(print-swype-matches "cghjkolkjhgredcftyuiokn")
