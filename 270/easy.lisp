(defun transpose-text (string-list)
  (flet ((pad (string new-length)
	   (format nil (format nil "~~~aa" new-length) string)))
    (let* ((max-length (reduce #'max (mapcar #'length string-list)))
	   (padded-list (mapcar (lambda (x) (pad x max-length)) string-list))
	   (max-y (length padded-list))
	   (max-x (length (nth 0 padded-list))))
      (mapcar (lambda (x) (string-right-trim " " x))
	      (loop for y below max-x collect
		   (concatenate 'string
				(loop for x below max-y collect
				     (elt (nth x padded-list) y))))))))

(defun split-seq (seq splitp)
  (loop
     for start = (position-if-not splitp seq)
     then (position-if-not splitp seq :start (1+ end))
     for end = (position-if splitp seq)
     then (position-if splitp seq :start start)
     if start collect (subseq seq start end)
     while end))

(defun split-lines (seq)
  (split-seq seq (lambda (x) (char= x #\Newline))))

(defun print-string-list (string-list)
  (format t "~{~a~%~}~%" string-list))

(defvar *challenge-input-1* "Some
text.")

(defvar *challenge-input-2* "package main

import \"fmt\"

func main() {
    queue := make(chan string, 2)
    queue <- \"one\"
    queue <- \"twoO\"
    close(queue)
    for elem := range queue {
        fmt.Println(elem)
    }
}")

(let ((split-input-1 (split-lines *challenge-input-1*))
      (split-input-2 (split-lines *challenge-input-2*)))
  (format t "Input 1:~%")
  (print-string-list split-input-1)
  (format t "Output 1:~%")
  (print-string-list (transpose-text split-input-1))
  (format t "Input 2:~%")
  (print-string-list split-input-2)
  (format t "Output 2:~%")
  (print-string-list (transpose-text split-input-2)))
